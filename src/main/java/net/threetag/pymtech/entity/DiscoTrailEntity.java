package net.threetag.pymtech.entity;

import net.minecraft.client.renderer.entity.LivingRenderer;
import net.minecraft.entity.*;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.IPacket;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.common.registry.IEntityAdditionalSpawnData;
import net.minecraftforge.fml.network.NetworkHooks;

public class DiscoTrailEntity extends Entity implements IEntityAdditionalSpawnData {

    public LivingEntity parent;
    public EntitySize size;
    public int lifeTime;
    public float limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, renderYawOffset;
    @OnlyIn(Dist.CLIENT)
    public LivingRenderer renderer;

    public DiscoTrailEntity(World worldIn) {
        super(PTEntityTypes.DISCO_TRAIL, worldIn);
        this.noClip = true;
        this.ignoreFrustumCheck = true;
    }

    public DiscoTrailEntity(EntityType<DiscoTrailEntity> entityType, World world) {
        super(entityType, world);
        this.noClip = true;
        this.ignoreFrustumCheck = true;
    }

    public DiscoTrailEntity(World worldIn, LivingEntity parent, int lifeTime) {
        super(PTEntityTypes.DISCO_TRAIL, worldIn);
        this.noClip = true;
        this.ignoreFrustumCheck = true;
        this.parent = parent;
        this.size = parent.getSize(parent.getPose());
        this.lifeTime = lifeTime;
        this.setLocationAndAngles(parent.getPosX(), parent.getPosY(), parent.getPosZ(), parent.rotationYaw, parent.rotationPitch);
    }

    @Override
    public EntitySize getSize(Pose poseIn) {
        return this.size != null ? this.size : super.getSize(poseIn);
    }

    @Override
    public void tick() {
        super.tick();
        if (this.ticksExisted >= this.lifeTime) {
            this.remove();
        }
    }

    @Override
    protected void registerData() {

    }

    @Override
    protected void readAdditional(CompoundNBT compound) {

    }

    @Override
    protected void writeAdditional(CompoundNBT compound) {

    }

    @Override
    public IPacket<?> createSpawnPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public void writeSpawnData(PacketBuffer buffer) {
        buffer.writeInt(this.lifeTime);
        buffer.writeInt(this.parent.getEntityId());
        buffer.writeFloat(this.size.width);
        buffer.writeFloat(this.size.height);
    }

    @Override
    public void readSpawnData(PacketBuffer additionalData) {
        this.lifeTime = additionalData.readInt();

        Entity en = this.world.getEntityByID(additionalData.readInt());
        if (en instanceof LivingEntity)
            this.parent = (LivingEntity) en;

        this.size = EntitySize.fixed(additionalData.readFloat(), additionalData.readFloat());
    }

    @OnlyIn(Dist.CLIENT)
    @Override
    public boolean isInRangeToRender3d(double x, double y, double z) {
        return true;
    }
}
