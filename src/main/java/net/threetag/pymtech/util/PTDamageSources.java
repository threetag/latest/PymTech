package net.threetag.pymtech.util;

import net.minecraft.util.DamageSource;

public class PTDamageSources {

    public static final DamageSource PYM_PARTICLE_SUFFOCATE = (new DamageSource("pymParticleSuffocate")).setDamageBypassesArmor();

}
